export default ({ app }, inject) => {
  
  const getBaserUrl = () => {
    return process.env.baseURL   
  };  

  inject("getBaserUrl", getBaserUrl);

}